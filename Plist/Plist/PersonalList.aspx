﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PersonalList.aspx.cs" Inherits="Plist.PersonalList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
 
        <asp:Label ID="lblName" runat="server" Text="Name:"></asp:Label>
&nbsp;<asp:TextBox ID="tbName" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="lblAge" runat="server" Text="Age:"></asp:Label>
&nbsp;&nbsp;
&nbsp;<asp:TextBox ID="tbAge" runat="server"></asp:TextBox>
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnAdd" runat="server"  Text="Add" OnClick="btnAdd_Click" />
        <br />
        <br />
        <asp:BulletedList ID="blList" runat="server">
        </asp:BulletedList>
        <br />
        <br />
    
    </div>
    </form>
</body>
</html>

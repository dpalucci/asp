﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Quiz2TravelMVC.Models
{
    public class Travel
    {
        [Key]
        public int Id { get; set; }

        [StringLength(50, MinimumLength=2)]
        [Required]
        public string  Name { get; set; }

        [Range(1,150)]
        public int Age { get; set; }

        [RegularExpression(@"^([A-Z]{2}[0-9]{6})$", ErrorMessage="Passport number must be in format AA123456")]
        public string PassportNo { get; set; }

        [RegularExpression(@"^([A-Z]{3})$", ErrorMessage="Airport code must be in format ABC")]
        public string FromAirport { get; set; }

        [RegularExpression(@"^([A-Z]{3})$", ErrorMessage="Airport code must be in format ABC")]
        public string ToAirport { get; set; }
        

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [CustomDate(ErrorMessage="You cannot be born in future :) and unfortunately you cannot be more than 150 years old")]
        public DateTime DepartDate { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [CustomDate(ErrorMessage = "You cannot be born in future :) and unfortunately you cannot be more than 150 years old")]
        public DateTime ArrivalDate { get; set; }
    
    }

    public enum ComfortClass
    {
        Economy, EconomyPlus, Premium, Business
    }

    public class TravelDBContext : DbContext
    {
        public DbSet<Travel> Travels { get; set; }
    }

    public class CustomDateAttribute : RangeAttribute
    {
        public CustomDateAttribute()
            : base(typeof(DateTime),
                    DateTime.Now.AddYears(-150).ToShortDateString(),
                    DateTime.Now.ToShortDateString())
        { }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace EFPeople
{
    public class Person
    {
        public Person()
        {

        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1. Add Person ");

            Console.WriteLine("2. List Persons ");
            Console.WriteLine("3. Delete Person by ID ");
            Console.WriteLine("4. Exit ");

            Console.ReadKey();
        }
    }
}

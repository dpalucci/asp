﻿using Quiz3AnimalCare2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Quiz3AnimalCare2.Controllers
{
    public class AnimalCareController : Controller
    {
        private LinqtoSqlDataContext context = new LinqtoSqlDataContext();
        // GET: AnimalCare
        public ActionResult Index()
        {           
            return View();
        }

        public ActionResult AnimalNameOrRace(string SearchString)
        {
            ViewBag.Heading = "All Animals types containing keyword";

            IList<Animals> carsList = new List<Animals>();
            var queryAnimalType = from a in context.Animals select a;                                 
                                  

            if (!String.IsNullOrEmpty(SearchString))
            {
                var queryAnimalType = queryAnimalType.Where(s => (s.Animals.RaceBreed.Contains(SearchString)
                    || s.Animals.Name.Contains(SearchString)));
                return View("Animals", queryAnimalType.ToList());
            }
        }

        public ActionResult Veterinarians()
        {
            ViewBag.Heading = "Find Vet by keyword";
           
        }

        public ActionResult FindByPostalCode()
        {
            ViewBag.Heading = "Animals by PostalCode";
            
        }
    }
}
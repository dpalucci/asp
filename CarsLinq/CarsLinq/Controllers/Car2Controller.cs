﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarsLinq.Controllers
{
    public class Car2Controller : Controller
    {
        //
        // GET: /Car2/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Car2/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Car2/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Car2/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Car2/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Car2/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Car2/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Car2/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewTrip.aspx.cs" Inherits="TripPlanner.NewTrip" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        Full Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tbFullName" runat="server"></asp:TextBox>
        &nbsp;&nbsp;
        <asp:RequiredFieldValidator ID="rfvName"
             runat="server" 
            ErrorMessage="Name must be provided."
            ControlToValidate="tbFullName"
            ForeColor="Red">
         </asp:RequiredFieldValidator>
        &nbsp;
        <asp:RegularExpressionValidator ID="revFullName" runat="server"
     ControlToValidate="tbFullName" 
            ErrorMessage="Name has to be greater than 2 and < 100 characters." ForeColor="Red" ValidationExpression="^.{2,100}$" >
        </asp:RegularExpressionValidator>
        <br />
        Gender:&nbsp;&nbsp;&nbsp;
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="rblGender" ForeColor="red" runat="server" ErrorMessage="You must select a gender. "></asp:RequiredFieldValidator>
&nbsp;<asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal">
            <asp:ListItem Value="NULL">N/A</asp:ListItem>
            <asp:ListItem Value="M">M</asp:ListItem>
            <asp:ListItem Value="F">F</asp:ListItem>
        </asp:RadioButtonList>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        Passport No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tbPassport" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvPassport"
             runat="server" 
            ErrorMessage="Passport # must be provided"
            ControlToValidate="tbPassport"
            ForeColor="Red">
         </asp:RequiredFieldValidator>
    
        &nbsp;
        <asp:RegularExpressionValidator ID="revPassport" ControlToValidate="tbPassport" ForeColor="red" runat="server" ValidationExpression="^[A-Z]{2}[0-9]{6}$" ErrorMessage="Invalid passport number, Format=AA123456"></asp:RegularExpressionValidator>
    
        <br />
        Date:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tbDate" TextMode="Date" runat="server"></asp:TextBox>
    
        &nbsp;
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="tbDate" ForeColor="red" runat="server" ErrorMessage="You must select a date, Format dd/mm/yyyy."></asp:RequiredFieldValidator>
    
        <br />
        Departure Airport:&nbsp;
        <asp:DropDownList ID="airportsFromList" runat="server" DataSourceID="SqlDataSource" DataTextField="airportName" DataValueField="airportCode">
        </asp:DropDownList><br />
        Arrival Airport:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="airportsToList" runat="server" DataSourceID="SqlDataSource" DataTextField="airportName" DataValueField="airportCode" >
        </asp:DropDownList>

    
        <asp:SqlDataSource ID="SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT code AS airportCode, (code + ' - ' + name) AS  airportName FROM [Airports]"></asp:SqlDataSource>
    
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnAddTrip" runat="server" Text="Add Trip" OnClick="btnAddTrip_Click" />
        <br />
        <br />
        <br />
        <br />
    
    </div>
    </form>
</body>
</html>

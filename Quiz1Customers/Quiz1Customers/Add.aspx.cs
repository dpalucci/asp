﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Quiz1Customers
{
    public partial class Add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;
            string name = tbName.Text;
            if (name.Length < 2 )
            {
                return;
            }
            string gender = rbGenderList.SelectedValue.ToString();
            string postalCode = tbPostalCode.Text;

            string email = tbEmail.Text;

            string province = ddlProvince.SelectedValue.ToString();
            

              //FIXME: Validate input

            try
            {

                SqlDataSource1.InsertCommand = "INSERT INTO Customers VALUES(@Name, @PostalCode, @Province, @Email, @Gender)";
                SqlDataSource1.InsertParameters.Add("Name", name );
                SqlDataSource1.InsertParameters.Add("PostalCode", postalCode);
                SqlDataSource1.InsertParameters.Add("Province", province);
                SqlDataSource1.InsertParameters.Add("Email", email);
                SqlDataSource1.InsertParameters.Add("Gender", gender);
                

                SqlDataSource1.Insert();


                Response.Write("<script>"
                + "alert('Congratulations, Customer has been successfully added!');"
                + "document.location = ('/Default.aspx');</script>)");
                

            }
            catch (SqlException ex)

            {

                Response.Write("<script>alert('Failed inserting record in the database');</script>");
                //Response.Write("<script>alert('Failed inserting record in the database. "
                //    + ex.Message + "');</script>");
                throw;

            }
            catch (InvalidOperationException)
            {
                Response.Write("<script>alert('Failed inserting record in the database');</script>");
            }
          




        }

        }
    }

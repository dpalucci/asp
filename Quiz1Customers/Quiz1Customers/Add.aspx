﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Quiz1Customers.Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="lblName"  runat="server" Text="Name:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tbName" runat="server"></asp:TextBox>
&nbsp;&nbsp;
        <asp:RegularExpressionValidator ControlToValidate="tbName" ForeColor="red" ID="RegularExpressionValidator1" runat="server" ValidationExpression="^.{2,}$" ErrorMessage="Name must be a minimum of 2 chars. "></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Label ID="lblPostalCode" runat="server" Text="Postal Code:"></asp:Label>
&nbsp;
        <asp:TextBox ID="tbPostalCode" runat="server"></asp:TextBox>
&nbsp;&nbsp;
        <br />
        <br />
        <asp:Label ID="lblProvinvce" runat="server" Text="Province:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="ddlProvince" runat="server" DataSourceID="SqlDataSource1" DataTextField="provinceName" DataValueField="Code">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Code , (Code + ' - ' + Name) AS  provinceName FROM [Provinces]"></asp:SqlDataSource>
        <br />
        <br />
        <asp:Label ID="lblEmail" runat="server" Text="Email:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tbEmail" runat="server"></asp:TextBox>
&nbsp;
        <br />
        <br />
        <asp:Label ID="lblGender" runat="server" Text="Gender: "></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:RadioButtonList ID="rbGenderList" runat="server" RepeatDirection="Horizontal" style="margin-left: 92px" Width="138px">
            <asp:ListItem Value="null">N/A</asp:ListItem>
            <asp:ListItem>M</asp:ListItem>
            <asp:ListItem>F</asp:ListItem>
        </asp:RadioButtonList>
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnAdd" runat="server" Text="Add Customer" OnClick="btnAdd_Click" />
        <br />
    
    </div>
    </form>
</body>
</html>

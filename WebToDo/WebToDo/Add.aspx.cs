﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebToDo
{
    public partial class Add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)

        {
            if (!Page.IsValid) return;

            string description = tbDescrition.Text;
            if (description.Length < 0 || description.Length > 100)
            {
                return;
            }

            DateTime dateTime;
            if (!DateTime.TryParse(tbDueDate, out dateTime)
                            || dateTime < DateTime.Today)
            {
                return;
            }

            SqlDataSource1.InsertCommandType = SqlDataSourceCommandType.Text;
            SqlDataSource1.InsertCommand = "INSERT INTO ToDos VALUES(@description, @dueDate)";

            SqlDataSource1.InsertParameters.Add("Description", description);
            SqlDataSource1.InsertParameters.Add("DateTime", dateTime.ToString());
            SqlDataSource1.Insert();

            tbDescrition.Text = "";
            tbDueDate.Text = "";
        }
    }
}
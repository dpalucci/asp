﻿using CarsMVCLinq.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarsMVCLinq.Context
{
    public class CarContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }

        CarContext db = new CarContext();

        // GET: /Owner/
        public ActionResult Index()
        {
            var owner = db.Cars.ToList();
            return View(owner);
        }

        //
        // GET: /Owner/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Owner/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Owner/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Owner/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Owner/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Owner/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Owner/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
﻿using CarsMVCLinq.Context;
using CarsMVCLinq.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarsMVCLinq.Controllers
{
    public class CarController : Controller
    {
        CarContext db = new CarContext();
        //
        // GET: /Car/
        public ActionResult Index()
        {
            var car = db.Cars.ToList();

            return View(car);
        }

        //
        // GET: /Car/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Car/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Car/Create
        [HttpPost]
        public ActionResult Create(Car car)
        {
            try
            {
                // TODO: Add insert logic here
                db.Cars.Add(car);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        
    }
}

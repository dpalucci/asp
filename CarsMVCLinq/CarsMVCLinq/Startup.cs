﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CarsMVCLinq.Startup))]
namespace CarsMVCLinq
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

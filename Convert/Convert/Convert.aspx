﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Convert.aspx.cs" Inherits="Convert.Convert" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="lblCelsius" runat="server" Text="Celsius"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tbCelsius" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" id="reqName" controltovalidate="tbCelsius" errormessage="Please enter celsius temp!" />

        <asp:Button ID="btnCtoF" runat="server" OnClick="btnCtoF_Click" Text="---&gt;" Width="76px" />
    </div>
    
        <br />
        <asp:Label ID="lblFarenheit" runat="server" Text="Farenheit"></asp:Label>

        &nbsp;&nbsp;

        <asp:TextBox ID="tbFarenheit" runat="server"></asp:TextBox>
        <asp:Button ID="btnFtoC" runat="server" OnClick="btnFtoC_Click"  Text="&lt;---" Width="77px" />
        <br />
        <br />
    </form>
</body>
</html>

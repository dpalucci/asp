﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Convert
{
    public partial class Convert : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCtoF_Click(object sender, EventArgs e)
        {
            double tempC = double.Parse(tbCelsius.Text);
            double faren = Math.Round((tempC * 1.8 + 32),1);
            tbFarenheit.Text = faren.ToString();
        }

        protected void btnFtoC_Click(object sender, EventArgs e)
        {
            double tempF = double.Parse(tbFarenheit.Text);
            double cel = Math.Round(((tempF - 32) / 1.8),1);
            tbCelsius.Text = cel.ToString();
        }
    }
}
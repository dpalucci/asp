﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace MVCPeopleEF.Models
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string PostalCode { get; set; }
        public string Gender { get; set; }
        public string BirthDate { get; set; }

    }

    public class PeopleDBContext : DbContext
    {
        public DbSet<Person> People { get; set; }
    }

}
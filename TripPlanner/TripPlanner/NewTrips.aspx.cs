﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TripPlanner
{
    public partial class NewTrips : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string name = tbName.Text;
            string gender = rbGenderlist.SelectedValue.ToString();
            string passportNo = tbPassport.Text;
            string date = tbDate.Text;
            string airportFrom = ddDepartAirport.SelectedValue.ToString();
            string airportTo = ddArrivalAirport.SelectedValue.ToString();

            //fixme validate input

            try
            {
                SqlDataSource1.InsertCommand = "INSERT INTO Trips VALUES(@Name, @Gender, @Passport, @Data, @FromAirport, @ArrivalAirport)";
                SqlDataSource1.InsertParameters.Add("Name", name );             
                SqlDataSource1.InsertParameters.Add("Gender", gender );
                SqlDataSource1.InsertParameters.Add("Passport", passportNo );
                SqlDataSource1.InsertParameters.Add("Date", date );
                SqlDataSource1.InsertParameters.Add("From", airportFrom );
                SqlDataSource1.InsertParameters.Add("To", airportTo );

                SqlDataSource1.Insert();

            }
            catch (SqlException ex)
            {
               // Response.Write("<script>alert('Failed inserting record in the database. " + ex.Message +);</script>)');
                Response.Write("<script>alert('Failed inserting record in the database. ');</script>)");
            }
            catch (InvalidOperationException)
            {
                Response.Write("<script>alert('Failed inserting record in the database. ');</script>)");
            }
        }
    }
}
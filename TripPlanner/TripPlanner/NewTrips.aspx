﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewTrips.aspx.cs" Inherits="TripPlanner.NewTrips" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <p>
            <br />
            <asp:Label ID="Label1" runat="server" Text="Full Name:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="tbName" runat="server"></asp:TextBox>
        </p>
        <p style="width: 783px">
            <asp:Label ID="Label2" runat="server" Text="Gender:"></asp:Label>
            <asp:RadioButtonList ID="rbGenderlist" runat="server" Height="16px"  Width="100px" RepeatDirection="Horizontal" style="margin-left: 1px">
                <asp:ListItem>M</asp:ListItem>
                <asp:ListItem>F</asp:ListItem>
                <asp:ListItem Value="Null">N/A</asp:ListItem>
            </asp:RadioButtonList>
&nbsp;
            </p>
        <p>
            <asp:Label ID="Label3" runat="server" Text="Passport No:"></asp:Label>
&nbsp;
            <asp:TextBox ID="tbPassport" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label4" runat="server" Text="Date:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="tbDate" TextMode="Date" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label5" runat="server" Text="Depart Airport Code:"></asp:Label>
&nbsp;
            <asp:DropDownList ID="ddDepartAirport" runat="server" DataSourceID="SqlDataSource1" DataTextField="airportName" DataValueField="airportName">
            </asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT code, (code +' - ' + name) AS airportName   FROM [Airports]"></asp:SqlDataSource>
        </p>
        <p>
            <asp:Label ID="Label6" runat="server" Text="Arrival Airport Code:"></asp:Label>
&nbsp;
            <asp:DropDownList ID="ddArrivalAirport" runat="server" DataSourceID="SqlDataSource1" DataTextField="airportName" DataValueField="airportName">
            </asp:DropDownList>
        </p>
        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Button ID="btnAdd" runat="server" Text="Add Trip" OnClick="btnAdd_Click" />
        </p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
    <div>
    
    </div>
    </form>
</body>
</html>
